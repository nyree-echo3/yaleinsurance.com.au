<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeederProject extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('modules')->insert([
            'name' => 'Project',
            'display_name' => 'Projects',
            'slug' => 'projects',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
