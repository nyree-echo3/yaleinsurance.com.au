<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CountriesTableSeederAustralia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('countries')->insert([
            'id' => 'AU',
            'name' => 'Australia',
            'phonecode' => '61',
            'is_eu_member' => 0           
        ]);
    }
}
