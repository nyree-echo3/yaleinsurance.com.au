<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>www.yaleinsurance.com.au</title>
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">   
    <link rel="stylesheet" href="{{ asset('css/site/holding.css') }}">
  </head>

  <body>
	  <table width="100%"  border="0" cellspacing="0">
	  <tr>
		<td> <div align="center">
		  <p class="style2"><a href='http://www.yaleinsurance.com.au'>www.yaleinsurance.com.au</a></p>
	      <p class="style2">&nbsp;</p>
		  <p class="style1">Contact person: Lee Baker, Insurance Director</p>
		  <p class="style1"></p>
		  <p class="style1"><strong>M</strong> 0455 172 111
			  <strong>E</strong> <a href='mailto:lee@yaleinsurance.com.au'>lee@yaleinsurance.com.au</a></p>
		  <p class="style1">Suite 1, 63 Ross Street, Toorak VIC 3142</p>
	      <p class="style1">&nbsp;</p>
		  <p class="style1">This website is being enhanced and updated by<a href="https://www.echo3.com.au"> echo3</a><a href="https://www.echo3.com.au"></a></p>
		  <p class="style1">&nbsp;</p>
		</div></td>
	  </tr>
	</table>
  </body>
</html>