<!DOCTYPE html>
<html>
<body>
<h1>New Claims Message User</h1>
<table class="table">
    @foreach(json_decode($claims_message->data) as $field)
        <tr>
            <th style="width:20%">{{ $field->field }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
