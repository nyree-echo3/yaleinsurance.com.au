<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-light btco-hover-menu navbar-custom">	
     <!--<div class="navbar-logo">
        <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/{{ (isset($page_type) ? "logo2.png" : "logo.png") }}" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
     </div>-->
     <!-- Flag Waving -->
	 @if (!isset($page_type))
	     <div class="logo-link logo-link-resp" onclick="logolink_onclick()"></div>
	     <div id="canvas" class="canvas-resp"></div>
	     <div class="plane plane-resp"><img id="logo-img" src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></div>     
     @else
	     <div class="logo-link-inner logo-link-inner-resp" onclick="logolink_onclick()"></div>
	     <div id="canvas" class="canvas-inner canvas-inner-resp"></div>
	     <div class="plane plane-inner plane-inner-resp"><img id="logo-img" class="logo-img-inner" src="{{ url('') }}/images/site/logo2.png" title="{{ $company_name }}" alt="{{ $company_name }}"></div>     
	
		 <div class="navbar-logo-txt">
			Insurance built around <span class="home-insurance-you"><strong>you</strong></span>
		 </div>
	 @endif
	
	<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<!--<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
			</li>-->
			@if(count($navigation))
               @foreach($navigation as $nav_item)    
					<li class="nav-item dropdown">
						<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('').'/'.$nav_item["url"] }}" id="navbarDropdownMenuLink">
							{{ $nav_item["display_name"] }}
						</a>
						
						@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 1)
							<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							    
							    @foreach($nav_item["nav_sub"] as $nav_sub)	
								
									<li><a class="dropdown-item" href="{{ url('').'/'.$nav_sub["url"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
									
									    @if(isset($nav_item["nav_sub_sub"]))
											<ul class="dropdown-menu">
												@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
												   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
													  <li><a class="dropdown-item" href="{{ url('').'/'.$nav_sub_sub["url"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
												   @endif
												@endforeach											
											</ul>
										@endif
										
									</li>
								@endforeach
								
							</ul>
						@endif
						
					</li>										
			   @endforeach
            @endif
            
            <li class="nav-item nav-item-college-capital">
			   <a class="nav-link" href="http://www.collegecapital.com.au" target="_blank">College Capital<br>Australia </a>
		    </li>                       			                        		
		</ul>			  		 									
	</div>
	
	<div class='navbar-contacts'>
		      <!--<div>Call us and let’s see how we can help <span class="home-insurance-you"><strong>you</strong></span></div>-->
		      <div><span class="home-insurance-you"><strong>Call us! It's that simple.</strong></span></div>
			  <a href='tel:{{ str_replace(' ', '', $phone_number) }}'>{{ $phone_number }}</a>			
			</div>
</nav>

<!--<div class='navbar-college-capital'>   
   <a href="http://www.collegecapital.com.au" target="_blank"><img src="{{ url('') }}/images/site/logo-college-capital-affiliated.png" title="College Capital Australia" alt="College Capital Australia"></a>		
</div>-->

<!-- Scripting for Logo Flag waving -->
<script id="plane-vs" type="x-shader/x-vertex">
	#ifdef GL_ES
	precision mediump float;
	#endif

	// those are the mandatory attributes that the lib sets
	attribute vec3 aVertexPosition;
	attribute vec2 aTextureCoord;

	// those are mandatory uniforms that the lib sets and that contain our model view and projection matrix
	uniform mat4 uMVMatrix;
	uniform mat4 uPMatrix;

	// if you want to pass your vertex and texture coords to the fragment shader
	varying vec3 vVertexPosition;
	varying vec2 vTextureCoord;

	void main() {
		vec3 vertexPosition = aVertexPosition;

		gl_Position = uPMatrix * uMVMatrix * vec4(vertexPosition, 1.0);

		// set the varyings
		vTextureCoord = aTextureCoord;
		vVertexPosition = vertexPosition;
	}
</script>
<script id="plane-fs" type="x-shader/x-fragment">
	#ifdef GL_ES
	precision mediump float;
	#endif

	// get our varyings
	varying vec3 vVertexPosition;
	varying vec2 vTextureCoord;

	// the uniform we declared inside our javascript
	uniform float uTime;

	// our texture sampler (default name, to use a different name please refer to the documentation)
	uniform sampler2D uSampler0;

	void main() {
// get our texture coords
		vec2 textureCoord = vTextureCoord;

		// displace our pixels along both axis based on our time uniform and texture UVs
		// this will create a kind of water surface effect
		// try to comment a line or change the constants to see how it changes the effect
		// reminder : textures coords are ranging from 0.0 to 1.0 on both axis
		const float PI = 3.141592;

		textureCoord.x += (
			sin(textureCoord.x * 10.0 + ((uTime * (PI / 3.0)) * 0.031))
			+ sin(textureCoord.y * 10.0 + ((uTime * (PI / 2.489)) * 0.017))
			) * 0.0075;

		textureCoord.y += (
			sin(textureCoord.y * 20.0 + ((uTime * (PI / 2.023)) * 0.023))
			+ sin(textureCoord.x * 20.0 + ((uTime * (PI / 3.1254)) * 0.037))
			) * 0.0125;

		gl_FragColor = texture2D(uSampler0, textureCoord);
	}
</script>

<script src="{{ url('') }}/js/site/curtains.min.js" type="text/javascript"></script>

<script>

window.onload = function() { 	
  $("#logo-img").fadeOut(1500);	
	
  // get our canvas wrapper
  var canvasContainer = document.getElementById("canvas");

  // set up our WebGL context and append the canvas to our wrapper
  var webGLCurtain = new Curtains("canvas");

  // get our plane element
  var planeElement = document.getElementsByClassName("plane")[0];

  // set our initial parameters (basic uniforms)
  var params = {
    vertexShaderID: "plane-vs", // our vertex shader ID
    fragmentShaderID: "plane-fs", // our framgent shader ID
    //crossOrigin: "", // codepen specific
    uniforms: {
      time: {
        name: "uTime", // uniform name that will be passed to our shaders
        type: "1f", // this means our uniform is a float
        value: 0,
      },
    }
  }

  // create our plane mesh
  var plane = webGLCurtain.addPlane(planeElement, params);

  // set up our basic methods
  plane.onRender(function() { // fired at each requestAnimationFrame call
    plane.uniforms.time.value++; // update our time uniform value
  });

}

function logolink_onclick() {	
	  window.location = "{{ url('') }}";
}
</script>