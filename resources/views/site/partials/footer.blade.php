<footer class='footer'> 
 <div class='footerContainer'>
  <div class="panelNav">
	  <div class="row">
		 <div class="col-lg-2">
			  <div id="footer-logo" >
				 <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-bottom.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			  </div>
		 </div>

		 <div class="col-lg-10">
		      <div id="footer-caption">
		        <span class="footer-caption-1">Managing your risks.</span>	
		        <span class="footer-caption-2">Insuring your rewards.</span>	
		      </div>		      		  
		      
			  <div id="footer-social">			    
				Follow us on 
				@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
				@if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
				@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
				@if ( $social_googleplus != "") <a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif
				@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
				@if ( $social_pinterest != "") <a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif
				@if ( $social_youtube != "") <a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube-square'></i></a> @endif
				<!--<a href="{{ url('') }}"/rss.xml" target="_blank"><i class='fa fa-rss'></i></a> 
				<a href="{{ url('') }}"/contact" ><i class='fa fa-envelope'></i></a>-->
				
				<div class="affilated">
				   Affiliated with 
				   <a href="http://www.collegecapital.com.au/" target="_blank">College Capital Australia </a>
				</div>
			  </div> 
		 </div>
	  </div>

	  <div id="footer-txt"> 
		@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a> | @endif 
		<a href="{{ url('') }}/pages/other/privacy-statement">Privacy Statement</a> |
		<a href="{{ url('') }}/pages/other/complaints">Complaints</a> |    
		<a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 
	  </div>
	  
	  <div class="footer-abn">
	  	ABN 42 628 992 101<br>
        AR no 1263317 of United Insurance Group AFSL 327131
	  </div>
    </div>
  </div>
</footer>