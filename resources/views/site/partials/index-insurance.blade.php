<div class="panelNav">
	<div class="home-insurance">
		@if(count($navigation))	      				   
		  @foreach($navigation as $nav_item)   
			@if ($nav_item["slug"] == "insurance")			           
				@if(isset($nav_item["nav_sub"])) 
				   <div class='home-insurance-container'>
					  <div class="row">	
						 @php
							$colCounter = 0;
						 @endphp

						 @foreach($nav_item["nav_sub"] as $nav_sub)	
						    @if ($nav_sub["thumbnail"] != "")
								@php
								   $colCounter++;
								@endphp

								<div class="col-lg-4 ">	
								   <a href='{{ url('') }}/pages/{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}'>				
									   <div class="home-insurance-a">
										 <div class="div-img">
											<img src="{{ url('') }}{{ $nav_sub["thumbnail"] }}" alt="{{ $nav_sub["title"] }}">
										 </div>
										 <div class="home-insurance-txt">
											{{ $nav_sub["title"] }}
										 </div>   
									   </div>							
								   </a>     						           						           
								</div>

								@if ($colCounter == 3)
									<div class="col-lg-12 home-insurance-caption">	
										<span>Insurance built around</span> <span class="home-insurance-you"><strong>you</strong></span>
									</div>
								@endif
							@endif
						 @endforeach
					  </div>
				   </div>
				@endif
			@endif
		 @endforeach	  
	  @endif
	</div>
</div>