@if(isset($home_news))
	 <div class="home-news">
	   <div class="container">
		  <div class="row">         
			 @foreach($home_news as $item)       	 
				  <div class="col-lg-4">
			           <h2>{{ $item->title }}</h2>
			           {!! $item->short_description !!}
			           
				       @if($item->thumbnail != "")
				          <div class="home-new-img">
					         <img class="rounded-circle" src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{ $item->title }}" width="140" height="140" />
					      </div>
					   @endif					   					 
					   					   
					   <p><a class="btn btn-secondary" href="{{ url('') }}/news/{{ $item->category->slug }}/{{ $item->slug }}" role="button">View details &raquo;</a></p>
				  </div><!-- /.col-lg-4 -->
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif