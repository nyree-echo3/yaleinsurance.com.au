<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Projects</h4>
	<ol class="navsidebar list-unstyled">
	  @foreach ($side_nav as $item)
		 <li class=''><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
	  @endforeach 	                                      
	</ol>		
  </div>          
</div>