<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Products</h4>
	<ol class="navsidebar list-unstyled">
       @if (isset($side_nav))	
	   @foreach ($side_nav as $item)
		 <li class=''><a class="navsidebar" href="{{ url('')."/".$item->url }}">{{ $item->name }}</a></li>
	   @endforeach 	        
	   @endif                              
	</ol>		
  </div>          
</div>