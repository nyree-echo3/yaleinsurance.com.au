@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-navigation')    
                     
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Claims</h1> 
               {!! $claims_details !!}                
            
               <form id="contact-form" method="post" action="{{ url('claims/save-message') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="claims-form-fields"></div>

                    <div class="form-row">
                        <div class="col-12 col-sm-10 g-recaptcha-container">
                            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn-submit">Submit Enquiry</button>
                </form>           
           </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    

@endsection
@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#claims-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('claims-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection
