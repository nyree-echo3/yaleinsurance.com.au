@extends('site/layouts/app')

@section('content')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-navigation')
                     
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Contact</h1>    
               <p>Thank you for your enquiry. We will be in touch with you shortly.</p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
<div><!-- /.blog-masthead -->    
@endsection            
