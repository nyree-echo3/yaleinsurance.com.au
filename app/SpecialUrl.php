<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialUrl extends Model
{

    protected $table = 'special_urls';
}
