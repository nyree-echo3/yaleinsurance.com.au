<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Kyslik\ColumnSortable\Sortable;
//use Rutorika\Sortable\SortableTrait;

class PropertyImage extends Model
{
    //use SortableTrait, Sortable;

    protected $table = 'property_images';

    //public $sortable = ['title', 'category_id', 'status'];  
}
