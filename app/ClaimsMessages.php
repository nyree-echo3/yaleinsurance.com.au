<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimsMessages extends Model
{
    protected $table = 'claims_messages';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('message-filter');
        $select = "";

        if($filter['search']){
            $select =  $query->where('name','like', '%'.$filter['search'].'%')
            ->orWhere('surname','like', '%'.$filter['search'].'%')
            ->orWhere('enquiry_details','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
