<?php

namespace App\Mail;

use App\ClaimsMessages;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClaimsMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $claims_message;

    public function __construct(ClaimsMessages $claims_message)
    {
        $this->claims_message = $claims_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName)
			        ->from($contactEmail)
			        ->view('site/emails/claims-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
					));
    }
}
