<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class DocumentCategory extends Model
{
    use SortableTrait;

    protected $table = 'document_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'category_id');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','documents')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'documents/'.$this->attributes['slug'];
    }
}
