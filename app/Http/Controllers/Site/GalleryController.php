<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Images;
use App\GalleryCategory;

class GalleryController extends Controller
{
    public function index(Request $request){

		$side_nav = $this->getCategories();			
		
		return view('site/gallery/list', array(         
			'side_nav' => $side_nav,			
        ));

    }
	
	public function detail($category_slug){

		$side_nav = $this->getCategories();
				
    	$category = $this->getCategory($category_slug);
		$items = $this->getItems($category->id);						
		
		return view('site/gallery/item', array(         
			'side_nav' => $side_nav,
			'category' => $category,			
			'items' => $items,			
        ));

    }
	
	public function getCategories(){
		$categories = GalleryCategory::whereHas("images")->with("images")->where('status', '=', 'active')->get();
        foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}
	
	public function getCategory($category_slug){
		$categories = GalleryCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}
	
	public function getItems($category_id){
		$items = Images::where('status', '=', 'active')->where('category_id', '=', $category_id)->orderBy('position', 'desc')->get();						
		return($items);
	}			
}
