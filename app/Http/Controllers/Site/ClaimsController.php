<?php

namespace App\Http\Controllers\Site;

use App\ClaimsMessages;
use App\Http\Controllers\Controller;
use App\Mail\ClaimsMessageAdmin;
use App\Mail\ClaimsMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

class ClaimsController extends Controller
{
    public function index(){

        $fields = Setting::where('key', '=', 'claims-form-fields')->first();

        // Claims Details
        $claims_details = Setting::where('key', '=', 'claims-details')->first();

        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/claims/claims', array(
			'page_type' => "Claims",
            'form' => $form,
            'claims_details' => $claims_details->value
        ));
    }
	
	public function quickclaims(){

        $fields = Setting::where('key', '=', 'quick-claims-form-fields')->first();
      
        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return ($form);
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contactclaims')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'claims-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ClaimsMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ClaimsMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ClaimsMessageAdmin($message));

        return \Redirect::to('claims/success');
    }
	
	public function saveMessageQuick(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contactclaims')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'quick-claims-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ClaimsMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ClaimsMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ClaimsMessageAdmin($message));

        //return \Redirect::to('claims/success');
		return view('site/claims/success');
    }

    public function success(){

        return view('site/claims/success');
    }
}
