<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ModulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $modules = Module::orderBy('position', 'desc')->get();
        return view('admin/modules/modules', array(
            'modules' => $modules
        ));
    }

    public function edit($module_id)
    {
        $module = Module::where('id', '=', $module_id)->first();
        return view('admin/modules/edit', array(
            'module' => $module
        ));
    }

    public function update(Request $request)
    {
        $rules = array(
            'display_name' => 'required'
        );

        $messages = [
            'display_name.required' => 'Please enter display name'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/modules/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $module = Module::where('id','=',$request->id)->first();
        $module->display_name = $request->display_name;
        $module->save();

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/modules/' . $module->id . '/edit')->with('message', Array('text' => 'Module has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/modules/')->with('message', Array('text' => 'Module has been updated', 'status' => 'success'));
        }
    }

    public function changeStatus(Request $request, $module_id)
    {
        $module = Module::where('id', '=', $module_id)->first();
        if ($request->status == "true") {
            $module->status = 'active';
        } else if ($request->status == "false") {
            $module->status = 'passive';
        }
        $module->save();

        return Response::json(['status' => 'success']);
    }

}