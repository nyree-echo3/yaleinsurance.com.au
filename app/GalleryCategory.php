<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class GalleryCategory extends Model
{
    use SortableTrait;

    protected $table = 'gallery_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function images()
    {
        return $this->hasMany(Images::class, 'category_id');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','gallery')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'gallery/'.$this->attributes['slug'];
    }
}
