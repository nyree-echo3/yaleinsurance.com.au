$(document).ready(function () {
    $( "#add-inspection" ).click(function() {

        var raw_inspections_data = $( "#inspections_data" ).val();
        var inspection_date = $( "#inspection_date" ).val();
        var inspection_date_arr = inspection_date.split("/");
        var inspection_date_sort = inspection_date_arr[2]+'/'+inspection_date_arr[1]+'/'+inspection_date_arr[0];
        var inspection_start_time = $( "#inspection_start_time" ).val();
        var inspection_end_time = $( "#inspection_end_time" ).val();
        var inspections_data = jQuery.parseJSON(raw_inspections_data);

        inspections_data.push({
            date: inspection_date,
            sort_date:inspection_date_sort,
            start_time: inspection_start_time,
            end_time: inspection_end_time
        });

        inspections_data.sort(inspection_sort);

        $( "#inspections_data" ).val(JSON.stringify(inspections_data));

        buildTable();
    });

    $('body').on('confirmed.bs.confirmation', '.delete-inspection', function() {
        deleteInspection($(this).data('key'));
    });

});

function deleteInspection(index){

    var raw_inspections_data = $( "#inspections_data" ).val();
    var inspections_data = jQuery.parseJSON(raw_inspections_data);
    inspections_data.splice(index, 1);
    $( "#inspections_data" ).val(JSON.stringify(inspections_data));
    buildTable();
}

function buildTable(){

    var raw_inspections_data = $( "#inspections_data" ).val();
    var inspections_data = jQuery.parseJSON(raw_inspections_data);
    var table_html = "";

    if(inspections_data.length>0){

        table_html = '<table id="inspections" class="table table-hover">'+
        '<tr>'+
        '<th>Date</th>'+
        '<th>Start Time</th>'+
        '<th>End Time</th>'+
        '<th></th>';

        for(var k in inspections_data) {

            table_html =  table_html+'</tr>'+
            '<tr>'+
            '<td>'+inspections_data[k].date+'</td>'+
            '<td>'+inspections_data[k].start_time+'</td>'+
            '<td>'+inspections_data[k].end_time+'</td>'+
            '<td>'+
            '<div class="pull-right">'+
            '<a class="tool delete-inspection" data-key="'+k+'" data-toggle=confirmation data-title="Are you sure?" data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>'+
            '</div>'+
            '</td>'+
            '</tr>';
        }
        table_html =  table_html+'</table>';
    }

    $( "#inspection_table_container" ).html(table_html);

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]'
    });
}

function inspection_sort(a, b) {
    return new Date(a.sort_date).getTime() - new Date(b.sort_date).getTime();
}