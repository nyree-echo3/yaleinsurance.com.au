  $(function () {

    $(".select2").select2();

    $("#pagination_count").select2({
        minimumResultsForSearch: -1
    });

    $("#pagination_count").change(function() {
        $("#pagination_count_form").submit();
    });

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        onConfirm: function() {
          $("#message-delete-form").submit();
        }
    });
    
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    $(".refresh-messages").click(function () {
      location.reload();
    });

    $(".checkbox-toggle").click(function () {
      var clicks = $(this).data('clicks');
      if (clicks) {

        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
      } else {

        $(".mailbox-messages input[type='checkbox']").iCheck("check");
        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
      }
      $(this).data("clicks", !clicks);
    });

    $(".mailbox-star").click(function (e) {
      e.preventDefault();

      var $this = $(this).find("a > i");
      var glyph = $this.hasClass("glyphicon");

      if (glyph) {
        $this.toggleClass("glyphicon-star");
        $this.toggleClass("glyphicon-star-empty");
      }


    });
});